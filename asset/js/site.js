/**
 * method sum array elements as result
 * return error if type of params is not array
 * */

const sum  = function ( array ) {
    if ( Array.isArray( array ) ){
        let result = 0;

        for ( let element of array ){
            result += element;
        }

        return result;

    } else {
        console.error('type of input params is not the array');
    }
}

/**
 * 
 * method for getting circle area
 * */

const circle = {

    circleRadius: 0,

    // radius getter
    get radius() {
        return this.circleRadius
    },

    // setter for set radius
    set radius( newRadius ) {
        this.circleRadius = newRadius;
    },

    // getting area
    get area() {
        return Math.PI * this.circleRadius ** 2;
    }

}
// example
circle.radius = 2;
console.log(circle.radius);
console.log(circle.area);