const circle = {
    radius: 1,
    draw() {
        console.log('draw');
    }    
};

// for (let key in circle)
//     console.log(key, circle[key]);

// for (let key of circle)
//     console.log('key');

// for (let key of Object.keys(circle))
//     console.log(key, circle[key]);

// built-in javascript engine function
// function Object() {};
// // const x = { value: 1 };
// const x =  new Object()

// log as array
// for (let entry of Object.entries(circle))
//     console.log(entry);


// const anotherCircle = {};

// for(let key in circle)
//     anotherCircle[key] = circle[key]

// extensions can be increased
// const anotherCircle = Object.assign({}, circle);

const anotherCircle = { ...circle};


console.log(anotherCircle)

// String primitive
const message = 'This is my\n first message';
// const message = 'Kiev, Lviv, Oddessa';

// const anotherMessage = new String('hi');

// console.log(message.length)
// console.log(message[2])
// console.log(message.includes('age'))
// console.log(message.startsWith('t'))
// console.log(message.endsWith('age'))
// console.log(message.indexOf('my'))
// console.log(message.replace('first', 'second'))
// let editedMessage = message.replace('first', 'second');
// console.log(message.toUpperCase())
// console.log(message.toLowerCase())
// console.log(message.trim())
// console.log(message.trimStart())
// console.log(message.split(', '))

// const name = 'John';
// const email = "Hi " + name +",\nThank you for joining my \"mailing\" list\nRegards,\nGiorgi"
// console.log(email)

// const email1 = `Hi ${name} ${2 + 3},
// Thank you for joining my "mailing" list

// Regards,
// Giorgi`

// console.log(email1)


const now = new Date();
const date1 = new Date('May 11 2023 09:00');
const date2 = new Date(2023, 4, 11, 9);

date2.setFullYear(2022);
console.log(date2);
console.log(date2.getMonth());
console.log(date2.getFullYear());
