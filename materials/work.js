let product = {
    title: 'Watermelon',
    price: 50,
    in_stock: true,
}

let fruits = ['cherry', 'banana'];
fruits[2] = 'Orange';

let user = {
    name: 'Alex',
    age: 34,
    like_music: true,
    gender: 'male'
}

user.age = 20;

function printUser( user ) {
    let subName = '';
    let status = '';
    let personName = '';
    if ( user.gender == 'male' ) {
        subName = 'He';
        personName = 'man';
    } else if ( user.gender === 'female' ) {
        subName = 'She';
        personName = 'woman';
    } else {
        subName = 'It'
        personName = 'item';
    }

    if ( user.age >= 30 ) {
        status = 'old';
    } else {
        status = 'yong';
    }

    console.log('This is ' + user.name + ', ' + subName + ' is ' + user.age + ' years old.');
    console.log( subName + ' is ' + status + ' ' + personName );
}

printUser( user );

function sumNumbers(firstNumber = 0, secondNumber = 0) {
    let result = firstNumber + secondNumber;
    if ( result === 0 ) {
        return 'You forgive to give me any number ;) '
    }
    return result;
}

function getMaxNumber(firstNumber, secondNumber) {
    return firstNumber === undefined || secondNumber === undefined ? 'You forget give me 2 numbers' : ( firstNumber === secondNumber ? "numbers are identical" : ( firstNumber > secondNumber ? firstNumber : secondNumber ) );
}

function swapValues(a, b) {
    let temp = a;
    a = b;
    b = temp;
    console.log(a);
    console.log(b);
}

function fizzBuzz(input) {

    if ( input % 3 === 0 && input % 5 === 0) {
        return 'fizzBuzz';
    }
    else if ( input % 3 === 0 && input % 5 !== 0 ) {
        return 'fizz';
    }
    else if ( input % 5 === 0 && input % 3 !== 0 ) {
        return 'buzz';
    } else  if ( isNaN(input) ) {
        return 'not a number';
    }

    return input;
}


function isLandScape(width, height) {
    return width > height;
}


/**
 * function return range array of numbers between min and max value including min & max
 */
function arrayFromRange(min, max) {
    const result = [];

    for (let i = min; i <= max; i++) {
        result.push(i);
    }

    return result;
}

/**
 * method duplicate standard JS function called 'includes()'
 */

function customIncludes(array, search) {
    for ( let element of array ) {
        if (element === search){
            return true;
        }

    }

    return false;
}

/**
 * method exclude all elements from first array same elements with same values from 2nd array
 * method build new array except elements of value of 2nd array
 */
function except(array, except) {
    let result = [];

    for ( let i = 0; i < array.length; i++ ){
        let findSameNumber = false;

        for ( let j = 0; j < except.length; j++ ){
            if ( array[i] === except[j] ){
                findSameNumber = true;
                break
            }
        }

        if (!findSameNumber) {
            result.push(array[i]);
        }
    }

    return result;
}





/**
 * shows conyt of stars on new string each time of iteration*/
function showStars(rows) {

    let star = '*';
    let outputString = '';
    let iteration = 0;
    for (let i = 0; i < rows; i++) {
        let nextStar = star ;
        for (let j = 0; j < iteration; j++) {
            nextStar = nextStar + star;
        }
        iteration = iteration + 1;
        outputString = outputString + nextStar + "\n";
    }

    console.log(outputString);
}


/**
 * method showPrimes
 * show only primes numbers before limitation*/
function isPrime(number) {
    if (number < 2) {
        return false;
    }

    for (let i = 2; i <= Math.sqrt(number); i++) {
        if (number % i === 0) {
            return false;
        }
    }

    return true;
}

function showPrimes(limit) {
    for (let num = 2; num <= limit; num++) {
        if (isPrime(num)) {
            console.log(num);
        }
    }
}


/**
 * method construct new address object*/
function createAddress(street, city, zipCode) {
    this.street = street;
    this.city = city;
    this.zipCode = zipCode;
}

const address = new createAddress('Soborniy avenue', 'Zaporizhzhya', '09605');
console.log(address);

/**
 * method create new blogPost*/
function createBlogPost(title, body, author, views, commentAuthor, commentBody) {
    let blogPost = {
        title: title,
        body: body,
        author: author,
        views: views,
        comments: {
            author: commentAuthor,
            body: commentBody,
        }
    }

    for (let key in blogPost)
        console.log(key, blogPost[key]);
}





/**
 * method delete element of array from old index and move it to new index
 * */
function move(array, index, offset) {

    if (Math.abs(offset) > array.length) {
        console.error('invalid offset');
        return array;
    }

    const newIndex = (index + offset + array.length) % array.length;

    // array.splice(newIndex, 0, ...array.splice(index, 1));

    const removedElement = array.splice(index, 1)[0];
    array.splice(newIndex, 0, removedElement);

    return array;

}

/**
 * method sum count of same elements in array
 * */
function countOcurences(array, search) {
    let count = 0;

    for (let element of array) {
        if (element === search) {
            count++;
        }
    }

    return count;
}

const person = {
    firstName: 'James',
    lastName: 'Bond',
    get fullName() {
        return `${person.firstName} ${person.lastName}`
    },
    set fullName(value) {
        if (typeof value !== 'string')
            throw new Error('Value in not a string');

        const parts = value.split(' ');
        if(parts.length !== 2)
            throw new Error('Enter first and last names');

        console.log(parts);
        this.firstName = parts[0];
        this.lastName = parts[1];
    }
}

// // console.log(person.fullName());

// // getters => access to properties
// // setters => change (mutate) properties
// console.log(person.fullName)

// // Try and catch
// try {
//     person.fullName = true;
// }
// catch(e) {
//     alert(e)
// }


// Scopes
// function start() {
//     const message = 'hi';
//     if (true) {
//         const another = 'bye';
//     }

//     for (let i = 0; i < 5; ++i) {
//         console.log(i)
//     }

//     console.log(i);
// }

// stop();

// const color = 'red';
// function start() {
//     const message = 'hi';
//     const color = 'blue'
//     console.log(message);
//     console.log(color);
// }

// function stop() {
//     const message = 'bye';
//     console.log(message);
// }

// start();
// // console.log(message);


// // Let vs var
// let x = 5;
// var y = 10;

// function start() {
//     for (var i = 0; i < 5; ++i) {
//         console.log(i);
//     }

//     console.log(i);
// }

// bofore ES6 (ES2015): var => function-scope variables
// ES6 (ES2015): let, const => block-scope variables

// var color = 'red';
// let age = 20;

// function sayHi() {
//     console.log('hi');
// }


// THIS keyword
// method -> obj
// function -> global (window)

// const video = {
//     title: 'a',
//     tags: ['a', 'b', 'c'],
//     showTags() {
//         const self = this;
//         this.tags.forEach(function(tag) {
//             console.log(self.title, tag);
//         });
//     }
// }

const video = {
    title: 'a',
    tags: ['a', 'b', 'c'],
    showTags() {
        this.tags.forEach(tag => console.log(this.title, tag)
        );
    }
}

video.showTags();

// video.stop = function() {
//     console.log(this.title)
// }

// video.stop();

// function playVideo() {
//     console.log(this);
// }

// playVideo();




/**
 * method sorting array of objects
 * */
const movies = [
    { title: 'a', year: 2023, rating: 4.5 },
    { title: 'b', year: 2023, rating: 4.7 },
    { title: 'c', year: 2023, rating: 3 },
    { title: 'd', year: 2022, rating: 4.5 }
];


function sortMovies( movies, year = 2023, rating = 4 ) {

    const filteredMovies = [];

    for( let movie of movies )
        if ( movie.year >= year && movie.rating >= rating ) {
            filteredMovies.push( movie );
        }

    const sortedMovies = filteredMovies.sort(compareByRating);

    sortedMovies.forEach(movie => console.log(movie.title));
}

function compareByRating(firstMovie, secondMovie) {
    return secondMovie.rating - firstMovie.rating;
}
// call to function
sortMovies(movies);