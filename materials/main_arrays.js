// Adding elements
// const numbers = [3, 4];
// console.log(numbers);
// // End
// numbers.push(5, 6);
// // Beginning
// numbers.unshift(1, 2);
// // Middle
// numbers.splice(2, 0, 'a', 'b');

// console.log(numbers);


// Finding elements
// Primitive types
const numbers = [1, 2, 3, 1, 1,4];

console.log(numbers.indexOf('x'))
console.log(numbers.indexOf(1))

console.log(numbers.lastIndexOf(1))

console.log(numbers.indexOf(1) !== -1);
console.log(numbers.includes('xyz'))


console.log(numbers.indexOf(1, 2));

// Reference types
const courses = [
    { id: 1, name: 'a', desc: 'abc' },
    { id: 2, name: 'b' },
    { id: 3, name: 'a' },
];

// console.log(courses.includes({ id: 1, name: 'a' }))
// courses.find()
const array1 = [5, 12, 8, 130, 44];
// console.log(array1);
// const found = array1.find(function(element) {
//     return element > 10
// });

// let course = courses.find(function(course) {
//     return course.name === 'a';
// });

// let course = courses.find(course => course.name === 'a');

// console.log(course);

// Removing elements
// End
// const last = array1.pop()
// console.log(last);
// // Beginning
// const first = array1.shift()
// console.log(first);
// // Middle
// array1.splice(1, 2);

// console.log(array1);


// Emptying array
let numArray = [1, 2, 3, 4];
let another = numArray;
console.log(numArray);
console.log(another);

// Solution 1
// numArray = [];

// Solution 2
// numArray.length = 0;

// Solution 3
// numArray.splice(0, numArray.length);

// Solutin 4
// while (numArray.length > 0) {
//     console.log(numArray);
//     numArray.pop();
// }

console.log(numArray);
console.log(another);