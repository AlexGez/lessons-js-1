

function checkSpeed(speed) {
    let speedMax = 70;
    if (speed <= speedMax) {
        console.log('OK');
    } else {
        let speedDiff = speed - speedMax;
        let points = 0;

        while (speedDiff >= 5) {
            speedDiff -= 5;
            points++;
        }

        if (points >= 12) {
            console.log('Призупинено дію водійських прав');
        } else {
            console.log('OK');
        }
    }
}

function showLimit(limit) {
    let numbers = [];

    while (limit !== 0) {
        if (limit % 2 === 0) {
            numbers.push(limit + ' "EVEN"');
        } else {
            numbers.push(limit + ' "ODD"');
        }
        limit--;
    }

    for (let i = numbers.length - 1; i >= 0; i--) {
        console.log(numbers[i]);
    }
}


function countTruthy(array) {
    let count = 0;

    for (let element of array) {
        if (element) {
            count++;
        }
    }

    console.log(count + ' Truthy элемента');
}

