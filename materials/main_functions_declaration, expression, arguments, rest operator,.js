// walk();
// // run();

// // Function Declaration
function walk() {
    alert('Are you sure you want to delete this message?');
}
walk()
// // Anonymous Function Expression
// const run = function() {
//     console.log('run');
// }

// // walk();
// // run();
// const move = run;
// move();


// Arguments
let a = 2;
a = 'x';

// function sum(a, b) {
//     console.log(arguments)
//     return a + b; // 1 + undefined = NaN
// }

// function sum() {
//     let total = 0;
//     for(let value of arguments)
//         total += value;

//     return total;
// }

// console.log(sum(1, 2, 3, 4, 5, 10))

// The Rest operator
// function sum(...args) {
//     return args.reduce((a, b) => a + b);
// }

// console.log(sum(1, 2, 3, 4, 5, 10));

// function discountSum(discount, ...prices) {
//     const total = prices.reduce((a, b) => a + b);
//     return total * (1 - discount);
// }

// console.log(discountSum(0.1, 20, 30, 50))


// Default parameters
// function interest(principal, rate, years) {
//     return principal * rate / 100 * years;
// }

// function interest(principal, rate, years) {
//     rate = rate || 14.5;
//     years = years || 5;
//     return principal * rate / 100 * years;
// }

function interest(principal, years, rate = 14.5,) {
    return principal * rate / 100 * years; 
}

console.log(interest(10000, 5));