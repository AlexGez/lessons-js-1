// console.log('Giorgi'); 

// let name = 'Giorgi';

// let name = 'Bond';
// console.log(name)

// Правила для задания имен переменных
// Не могут использовать ключевые слова let, if, else, const etc.
// Должно иметь значение 
// Не может начинаться с числа 1name
// Не может содержать пробелы или (-)
// Переменные являются регистро-зависимыми
// let firstName = 'James';
// let lastName = 'Bond';
// let LastName = 'John';
// console.log(lastName)

// let name = 'Bond';
// let age;
// console.log(name, age)

// const interestValue = 0.3;
// // interestValue = 1;
// console.log(interestValue)

// Primitive types
// String
// Number
// Boolean true, false
// Undefined
// Null

// let name = 'Giorgi';
// let age = String(55);

// let person = {
//     name: 'Giorgi',
//     age: 55,
// }


// // Dot Notation
// // console.log(person.age, person.name);
// let type = 'name'

// person.name = 'James'
// //Bracket Notation
// console.log(person)


// Array
// let selectedColors = ['red', 'blue'];
// selectedColors[2] = 1;
// console.log(typeof selectedColors[2]);

// Functions
// Performing a task
// function greet(name, lastName) {
//     console.log('Hello ' + name + ' ' + lastName);
// }

// greet('Giorgi', 'Kobalava');

// Calculating a value
// function square(number) {
//     return number * number;
// }

// let squareNumber = square(4)
// console.log(square(2));
// console.log(squareNumber);

// Operators

// let x = 10;
// let y = 4;

// console.log(x + y);
// console.log(x - y);
// console.log(x * y);
// console.log(x / y);
// console.log(x % y); // remaining of devision
// console.log(x ** y);


// Increment (++)
// console.log(++x);
// console.log(x++);
// x = x + 1;
// x = x + 5;
// x += 5
// x = x * 3
// x *= 3
// console.log(x);



// Decrement (--)
// console.log(--x);

// Assignment Operators
// let x = 10;
// x++;
// x += 5;


// Comparison Operators
// let x = 1;
// Relational
// console.log(x > 0);
// console.log(x >= 0);
// console.log(x < 1);
// console.log(x <= 1);

// Equality 
//Strict Equantity (Type + Value)
// console.log(1 === 1);
// console.log(1 !== 1);
// console.log('1' === 1);

// Lose Equality (Value)
// console.log(1 == 1);
// console.log(1 != 1);
// console.log('1' == 1);


